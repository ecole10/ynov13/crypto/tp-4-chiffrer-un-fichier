# TP 4 - Chiffrer un fichier

## Commandes

openssl dgst -sha1 fichier.txt

openssl enc -e -aes-256-cbc -in fichier.txt -out fichier-chiffre.txt

openssl enc -d -aes-256-cbc -in fichier-chiffre.txt -out fichier.txt
